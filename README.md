# AZsPCs Redux

TODO explain can be used as web or Deno.

## Web

https://tdillon.gitlab.io/azspcs-redux/

## Deno

_Install_ or *update* the CLI with the following command.

```shell
deno install --allow-net=api.dropboxapi.com,content.dropboxapi.com,gitlab.com,0.0.0.0 \
             --allow-read=. \
             --name azspcs \
             --force \
             https://gitlab.com/api/v4/projects/tdillon%2fazspcs-redux/packages/generic/deno-cli/0.0.28/azspcs.js
```

Once _installed_, execute with

```shell
azspcs
```

## Development

### CLI

```shell
deno run --allow-net=api.dropboxapi.com,content.dropboxapi.com,0.0.0.0 \
         --allow-read=. \
         src/cli.ts
```

### Web

```shell
deno task build:local

# Serve the `azspcs-redux` directory.  e.g., using http-server NPM package:
http-server -c-1

# Now open a browser and navigate to appropriate URL.  e.g., localhost:8080
```

### Authenication Workflow

```mermaid
graph TD
    Start
    Authenticated((Successfully authenticated))
    CheckLocalStorage{"Does token exist?"}
    RemoveLocalStorage["Remove token"]
    CheckToken{"Test Token.  Success?"}
    IsDeno{"Is Deno app?"}
    DenoGetAuthCode["get auth code"]
    BrowserCodeInUrl{"code in url"}
    BrowserShowLink["show link"]
    GetAndSaveToken["get and save token"]

    Start --> CheckLocalStorage
    CheckLocalStorage -->|true| CheckToken
    CheckLocalStorage -->|false| IsDeno
    IsDeno --> |true| DenoGetAuthCode
    DenoGetAuthCode --> GetAndSaveToken
    IsDeno --> |"false (i.e., browser)"| BrowserCodeInUrl
    BrowserCodeInUrl --> |true| GetAndSaveToken
    BrowserCodeInUrl --> |false| BrowserShowLink
    RemoveLocalStorage & GetAndSaveToken & BrowserShowLink --> Start
    CheckToken --> |true| Authenticated
    CheckToken --> |false| RemoveLocalStorage
```

### Logic Sequence

```mermaid
sequenceDiagram
    participant W as worker.ts
    participant I as Web/CLI
    participant U as util.ts
    participant C as i-contest.ts
    I->>U: getBest('contest-name')
    U->>U: get all Dropbox files for contest
    U->>C: all bests from Dropbox
    C->>U: canonical bests
    U->>U: set bests in Dropbox
    U->>I: [{n:'17',solution:'1,39,...'}, ...]
    I->>W: {c: 'contest-name', n: '23', bests: [{...},...]}
    loop forever
        par seach for bests
            W->>W: search for better solution
        and save bests to Dropbox
            W->>I: found new best -> N,solution
            I->>U: save new best
        end
    end

```

### Delete a Release

```shell
curl --header "PRIVATE-TOKEN: <TOKEN_WITH_API_PERM>" -X DELETE https://gitlab.com/api/v4/projects/tdillon%2fazspcs-redux/releases/<TAG>
```
