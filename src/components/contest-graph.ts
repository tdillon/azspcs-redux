import { css, customElement, html, LitElement, property, query } from '../deps.web.ts'
import { ContestClass } from '../contests/i-contest.ts'

@customElement('contest-graph')
export class ContestGraph extends LitElement {

    static styles = css`
        :host { display: block; }
        canvas { width: 100%; display: block; }
    `;

    @property()
    contestClass!: ContestClass

    @property()
    myBests!: Array<number>

    // @ts-ignore WHY!!!!
    @query('canvas', true) cvs: HTMLCanvasElement

    ctx!: CanvasRenderingContext2D

    render() {
        return html`<canvas></canvas>`
    }

    override updated() {
        this.#draw()
    }

    #draw() {
        // Using this method to ensure the canvas element exists.
        this.ctx = this.cvs.getContext('2d') as CanvasRenderingContext2D
        this.cvs.width = this.cvs.clientWidth * window.devicePixelRatio
        this.cvs.height = this.cvs.width / 2
        this.cvs.style.width = `${this.cvs.clientWidth}px`
        this.cvs.style.height = `${this.cvs.clientHeight}px`

        this.ctx.clearRect(0, 0, this.cvs.width, this.cvs.height)

        const segmentWidth = this.cvs.width / this.contestClass.KNOWN_BESTS.length
        const max = Math.max(this.myBests.reduce((prev, cur) => Math.max(prev, cur)), this.contestClass.KNOWN_BESTS.reduce((prev, cur) => Math.max(prev, cur)))
        const min = Math.min(this.myBests.reduce((prev, cur) => Math.min(prev, cur)), this.contestClass.KNOWN_BESTS.reduce((prev, cur) => Math.min(prev, cur)))
        const range = max - min

        // known bests
        this.ctx.fillStyle = '#09c'
        for (let i = 0; i < this.contestClass.KNOWN_BESTS.length; ++i) {
            const n = this.contestClass.KNOWN_BESTS[i]
            this.ctx.beginPath()
            this.ctx.arc(segmentWidth * i + (segmentWidth / 2), this.cvs.height - (n - min) * this.cvs.height / range, 10, 0, 2 * Math.PI)
            this.ctx.fill()
        }

        // my bests
        this.ctx.fillStyle = '#f00'
        for (let i = 0; i < this.myBests.length; ++i) {
            const n = this.myBests[i]
            this.ctx.beginPath()
            this.ctx.arc(segmentWidth * i + (segmentWidth / 2), this.cvs.height - (n - min) * this.cvs.height / range, 5, 0, 2 * Math.PI)
            this.ctx.fill()
        }
    }

}
