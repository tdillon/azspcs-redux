import { ContestClass } from '../contests/i-contest.ts'
import { customElement, html, LitElement, property, state, css } from '../deps.web.ts'
import { getCanonicalBestSolutions } from '../utilities/dropbox.ts'
import { CONTEST_CLASS_MAPPING } from '../utilities/util.ts'
import { NEW_BEST_SCORE_EVENT_DATA } from './contest-runners.ts'

import './contest-graph.ts'
import './contest-runners.ts'
import './contest-table.ts'

@customElement('contest-page')
export class ContestPage extends LitElement {


    static styles = css`
        :host { display: block; }

        contest-table, contest-graph {
            margin-bottom: 3em;
            background-color: #222;
            border-top: solid 1px #666;
            border-bottom: solid 1px #666;
        }

        h1 {
            font-size: 18px;
            padding: 1em .5em;
            margin:0;
        }

        p {
            padding: 1em;
        }
    `;


    @property()
    contest = ''

    private contestClass!: ContestClass
    private bestSolutionsForContest!: string[]

    @state()
    private myBestScores!: Array<number>

    @state()
    private tableData!: Array<{ n: number, mybest: number, knownbest: number, ratio: number, solution: string }>

    render() {
        if (this.contest) {
            return html`
                <h1>${this.contestClass.NAME}</h1>${this.bestSolutionsForContest
                    ? html`
                <contest-runners .contest=${this.contest} .myBestSolutions=${this.bestSolutionsForContest}></contest-runners>
                <contest-table .tableData=${this.tableData}></contest-table>
                <contest-graph .myBests=${this.myBestScores} .contestClass=${this.contestClass}></contest-graph>`
                    : html`
                <p>waiting on data to load</p>`}`
        } else {
            return html`<div><code>contest</code> property is required for <code>contest-table</code> element.</div>`
        }
    }

    private listenerCurrent = (evt: CustomEvent<NEW_BEST_SCORE_EVENT_DATA>) => {
        console.log(`[contest-page] - received azspcsReduxNewBestScore event - ${JSON.stringify(evt.detail)}`)

        if (evt.detail.score > this.myBestScores[this.contestClass.N.indexOf(evt.detail.n)]) {
            console.log(`[contest-page] this is a better solution, updating now`)

            this.myBestScores[this.contestClass.N.indexOf(evt.detail.n)] = evt.detail.score

            this.myBestScores = [...this.myBestScores]

            const td = this.tableData.find(d => d.n === evt.detail.n)!;
            td.mybest = evt.detail.score
            td.ratio = td.mybest / td.knownbest
            td.solution = evt.detail.solution

            this.tableData = [...this.tableData]
        } else {
            console.log(`[contest-page] this is a worse solution, no updates`)
        }
    }

    async connectedCallback() {
        super.connectedCallback()

        this.contestClass = CONTEST_CLASS_MAPPING.get(this.contest)!
        this.bestSolutionsForContest = await getCanonicalBestSolutions(this.contest)
        this.myBestScores = this.contestClass.N.map(n => this.contestClass.getBestScore(n, this.bestSolutionsForContest))

        this.tableData = this.contestClass.N.map(n => ({
            n,
            mybest: this.contestClass.getBestScore(n, this.bestSolutionsForContest) || 0,
            knownbest: this.contestClass.KNOWN_BESTS[this.contestClass.N.findIndex(num => num === n)] || -1,
            ratio: -1,
            solution: this.contestClass.getSolution(n, this.bestSolutionsForContest) || ''
        }))

        this.tableData.forEach(d => d.ratio = d.mybest / d.knownbest)

        self.addEventListener('azspcsReduxNewBestScore', this.listenerCurrent)
    }

    disconnectedCallback() {
        self.removeEventListener('azspcsReduxNewBestScore', this.listenerCurrent)

        super.disconnectedCallback()
    }
}
