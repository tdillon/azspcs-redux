import { customElement, html, LitElement, state } from '../deps.web.ts';
import { authenticate, DROPBOX_AUTHORIZE_URL } from '../utilities/dropbox.ts'

export type DropboxAuthEventType = 'WAITING' | 'AUTHENTICATED' | 'UNAUTHENTICATED'

@customElement('dropbox-auth')
export class DropboxAuth extends LitElement {

    @state() authstate: 'UNKNOWN' | DropboxAuthEventType = 'UNKNOWN'

    render() {
        switch (this.authstate) {
            case 'AUTHENTICATED': return html`<div>Successfully authenticated with Dropbox.</div>`
            case 'UNAUTHENTICATED': return html`<a href=${DROPBOX_AUTHORIZE_URL}>Authenticate with Dropbox</a>`
            case 'WAITING': return html`<div>Checking Dropbox authentication...</div>`
            case 'UNKNOWN': return html`${this.authstate}`
        }
    }

    override async firstUpdated() {
        try {
            this.authstate = 'WAITING'
            dispatchEvent(new CustomEvent<DropboxAuthEventType>('dropboxAuthChange', { detail: 'WAITING', bubbles: true, composed: true, cancelable: true }))
            await authenticate()
            this.authstate = 'AUTHENTICATED'
            dispatchEvent(new CustomEvent<DropboxAuthEventType>('dropboxAuthChange', { detail: 'AUTHENTICATED', bubbles: true, composed: true, cancelable: true }))
        } catch (_error) {
            console.log('[BROWSER] exception from authentication attempt.  User will need to authenticate with dropbox.');
            this.authstate = 'UNAUTHENTICATED'
            dispatchEvent(new CustomEvent<DropboxAuthEventType>('dropboxAuthChange', { detail: 'UNAUTHENTICATED', bubbles: true, composed: true, cancelable: true }))
        }
    }
}

declare global {
    interface WindowEventMap {
        'dropboxAuthChange': CustomEvent<DropboxAuthEventType>
    }
}
