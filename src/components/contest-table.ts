import { customElement, html, LitElement, property, css } from '../deps.web.ts';

@customElement('contest-table')
export class ContestTable extends LitElement {

    static styles = css`
        :host {
            display: block;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            width:1%;
            padding: 6px;
            text-align: center;
        }

        th {
            border-bottom:solid 2px #333;
        }

        th:last-child {
            text-align: left;
        }

        td:last-child {
            width:100%;
            text-align: left;
        }

        input[type="text"] {
            width:100%;
            font-family:sans-serif;
            font-size:.9em;
            overflow: hidden;
            text-overflow: ellipsis;
            background: transparent;
            border:none;
            color:white;
        }

        input[type="button"] {
            background: #333;
            border: solid 1px #666;
            line-height: 1em;
            padding: 0.25em;
        }
    `

    @property()
    tableData: Array<{ n: number, mybest: number, knownbest: number, ratio: number, solution: string }> = []

    render() {
        return html`
            <table>
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>N</th>
                        <th>My Best</th>
                        <th>Known Best</th>
                        <th>Ratio</th>
                        <th>Solution</th>
                    </tr>
                </thead>
                <tbody>${this.tableData.map(data => html`
                    <tr>
                        <td><input type=button value=➕ @click=${() => this.#addNewRunner(data.n)}></td>
                        <td>${data.n}</td>
                        <td>${data.mybest}</td>
                        <td>${data.knownbest}</td>
                        <td>${data.ratio.toFixed(2)}</td>
                        <td><input type=text readonly value=${data.solution} @click=${(e: MouseEvent) => (e.target as HTMLInputElement).select()}></td>
                    </tr>`)}
                </tbody>
            </table>
        `
    }

    #addNewRunner(n: number) {
        dispatchEvent(new CustomEvent('azspcsReduxNewRunner', { detail: n, bubbles: true, composed: true, cancelable: true }))
    }

}

declare global {
    interface WindowEventMap {
        'azspcsReduxNewRunner': CustomEvent<number>
    }
}
