import { css, customElement, html, LitElement, property, state } from '../deps.web.ts'
import { IClientMessage, IWorkerMessage } from '../sw.ts'
import { saveSolution } from '../utilities/dropbox.ts'

interface RUNNER_NUM_AND_WORKER {
    n: number
    w: Worker
}

export interface NEW_BEST_SCORE_EVENT_DATA {
    n: number
    score: number
    solution: string
}

@customElement('contest-runners')
export class ContestRunners extends LitElement {

    static styles = css`
        :host {
          display: block;
        }

        input[type="button"] {
            color: white;
            font-size: 1.5em;
            margin: 0 .5em 1em;
            line-height: 1em;
            background: #333;
            border: solid 1px #666;
            line-height: 1em;
            padding: 0.25em;
        }
    `

    @property()
    contest = ''

    @property()
    myBestSolutions!: Array<string>

    @state()
    private runners: Array<RUNNER_NUM_AND_WORKER> = []

    render() {
        return html`${this.runners.map(r => html`<input type=button value=${`❌${r.n}`} @click=${() => this.#stopRunner(r)}>`)}`
    }

    #stopRunner(runner: RUNNER_NUM_AND_WORKER) {
        console.log(`[contest-runner] stopping ${runner.n}`)
        runner.w.terminate()
        this.runners.splice(this.runners.findIndex(r => r === runner), 1)
        this.requestUpdate()
    }

    private listenerCurrent = (evt: CustomEvent<number>) => {
        const w = new Worker('sw.js', { type: 'module' })

        this.runners.push({ n: evt.detail, w })

        w.addEventListener('message', async (e: MessageEvent<IWorkerMessage>) => {
            if (e.data.type === 'INFO') {
                console.log(`[contest-runner] Message from worker: ${e.data.message}`)
            } else if (e.data.type === 'SOLUTION') {
                console.log(`[contest-runner] Found new best for ${e.data.contest} - ${e.data.n.toString()} - ${e.data.score} - ${e.data.solution}`)
                await saveSolution(this.contest, e.data.solution)
                console.log(`[contest-runner] dispatching azspcsReduxNewBestScore event now`)
                dispatchEvent(new CustomEvent<NEW_BEST_SCORE_EVENT_DATA>('azspcsReduxNewBestScore', { detail: e.data, bubbles: true, composed: true, cancelable: true }))
            } else {
                throw new Error(`[contest-runner] message event was received from worker with bad data:  ${JSON.stringify(e.data)}`)
            }
        })

        w.postMessage(<IClientMessage>{ contest: this.contest, arg: evt.detail.toString(), bests: this.myBestSolutions })

        this.requestUpdate()
    }

    connectedCallback() {
        super.connectedCallback()

        self.addEventListener('azspcsReduxNewRunner', this.listenerCurrent)
    }

    disconnectedCallback() {
        self.removeEventListener('azspcsReduxNewRunner', this.listenerCurrent)

        super.disconnectedCallback()
    }
}


declare global {
    interface WindowEventMap {
        'azspcsReduxNewBestScore': CustomEvent<NEW_BEST_SCORE_EVENT_DATA>
    }
}
