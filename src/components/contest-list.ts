import { customElement, html, LitElement, css } from '../deps.web.ts';
import { CONTESTS } from '../utilities/util.ts'

@customElement('contest-list')
export class ContestList extends LitElement {

    static styles = css`
        a { color: white; }
    `

    render() {
        return html`
            <ul>
                ${CONTESTS.map(c => html`<li><a href=${c}>${c}</a></li>`)}
            </ul>
        `

    }
}
