import { CONTEST_CLASS_MAPPING } from './utilities/util.ts'

export type IWorkerMessage = {
    type: 'INFO'
    message: string
} | {
    type: 'SOLUTION'
    contest: string
    n: number
    score: number
    solution: string
}

export interface IClientMessage {
    contest: string
    arg: string | boolean
    bests: Array<string>
}

const ctx: Worker = self as unknown as Worker;

ctx.postMessage(<IWorkerMessage>{ type: 'INFO', message: 'Worker is ready to begin.' });

ctx.addEventListener('message', (e: MessageEvent<IClientMessage>) => {
    console.log(`[WORKER] Received message to solve ${e.data.contest} - ${e.data.arg}`)

    const contestClass = CONTEST_CLASS_MAPPING.get(e.data.contest)

    if (contestClass === undefined) {
        throw new Error(`[WORKER] cannot find contest ${e.data.contest}`)
    }

    const contest = new contestClass(typeof e.data.arg === 'boolean' ? undefined : +e.data.arg, e.data.bests);

    console.log(`[WORKER] Begin infinite solve loop for ${contestClass.NAME} - ${e.data.arg}`)

    do {
        if (contest.solve()) {
            console.log(`[WORKER] Found new best of ${contest.getScore()} for ${contestClass.NAME} - ${e.data.arg}`);
            ctx.postMessage(<IWorkerMessage>{ type: 'SOLUTION', contest: e.data.contest, n: +e.data.arg, score: contest.getScore(), solution: contest.toString() });
        }
    } while (true);
});
