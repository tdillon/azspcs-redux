export { html, LitElement, css }                 from 'https://cdn.skypack.dev/lit@2.3.1?dts';
export { when }                                  from 'https://cdn.skypack.dev/lit@2.3.1/directives/when?dts'
export { customElement, property, state, query } from 'https://cdn.skypack.dev/lit@2.3.1/decorators?dts'

export { contextProvided, contextProvider, createContext } from 'https://cdn.skypack.dev/@lit-labs/context@0.1.3?dts'

export { Routes, Router } from 'https://cdn.skypack.dev/@lit-labs/router@0.1.1?dts'

// Needed until Firefox implements URLPattern (assuming you want to test using Firefox)
// https://developer.mozilla.org/en-US/docs/Web/API/URLPattern
export {  URLPattern } from 'https://esm.sh/urlpattern-polyfill@5.0.5?no-dts'
