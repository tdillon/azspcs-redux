import { html, LitElement, customElement, state, Router, css } from './deps.web.ts'

import './components/dropbox-auth.ts'
import './components/contest-list.ts'
import './components/contest-page.ts'
import { DropboxAuthEventType } from './components/dropbox-auth.ts'

@customElement('my-app')
export class App extends LitElement {

    static styles = css`:host { display: block; }`

    @state()
    private isAuthenticated = false

    private _router = new Router(this, [
        { path: '/azspcs-redux/', render: () => html`<contest-list></contest-list>` },
        { path: '/azspcs-redux/index.html', render: () => html`<contest-list></contest-list>` },
        { path: '/azspcs-redux/:contest', render: ({ contest }) => html`<contest-page .contest=${contest}></contest-page>` },
    ])

    render() {
        return html`
            ${this.isAuthenticated ?
            html`${this._router.outlet()}` :
            html`<dropbox-auth></dropbox-auth>`}
        `
    }

    #dropboxAuthChangeListener = (p: CustomEvent<DropboxAuthEventType>) => this.isAuthenticated = (p.detail === 'AUTHENTICATED')

    connectedCallback() {
        super.connectedCallback()
        self.addEventListener('dropboxAuthChange', this.#dropboxAuthChangeListener)
    }

    disconnectedCallback() {
        self.removeEventListener('dropboxAuthChange', this.#dropboxAuthChangeListener)
        super.disconnectedCallback()
    }
}
