import { VERSION } from '../version.ts'
import { CONTESTS, CONTEST_CLASS_MAPPING } from './util.ts'

export const HELP = `
azspcs ${VERSION}

A CLI for running my AZsPCs contests.

Check for latest versions here https://gitlab.com/tdillon/azspcs-redux/-/releases

Usage:
  azspcs (help | --help)
  azspcs (version | --version)
  azspcs bests --<contest-name>...
  azspcs run --<contest-name>=<contest-n>...

Subcommands:
  bests    Output the bests for contest(s).
           Also canonicalizes Dropbox files.
           The acceptable --<contest-name> arguments are:
               ${CONTESTS.map(c => `--${c}`).join('\n               ')}
  help     Print this help message.
  run      Run solver for a given contest.
           Any number of --<contest-name>=<contest-n> can be provided.
           Here are the acceptable --<contest-name>=<contest-n> arguments:
               ${CONTESTS.flatMap(c => CONTEST_CLASS_MAPPING.get(c)?.N.map(n => `--${c}=${n}`)).join('\n               ')}
  version  Show version info and exit.

Options:
  --help     Print this help message.
  --version  Show version info and exit.

Examples:
  Print this help message.
  $ azspcs help
  $ azspcs --help

  Print the version information.
  $ azspcs version
  $ azspcs --version

  Output the bests for given contests.  Also canonicalizes Dropbox files.
  $ azspcs bests --${CONTESTS[Math.floor(Math.random() * CONTESTS.length)]}
  $ azspcs bests ${CONTESTS.map(c => `--${c}`).join(' ')}

  Run solver.  Add as many --<contest-name>=<contest-n> as desired.
  $azspcs run ${[Math.floor(Math.random() * CONTESTS.length), Math.floor(Math.random() * CONTESTS.length)].map(_ => {
    const randomN = Math.floor(Math.random() * (CONTEST_CLASS_MAPPING.get(CONTESTS[_])?.N.length || 0))
    return `--${CONTESTS[_]}=${CONTEST_CLASS_MAPPING.get(CONTESTS[_])?.N[randomN]}`
}).join(' ')}
`