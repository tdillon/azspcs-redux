import { delay } from '../deps.ts'
import { CONTEST_CLASS_MAPPING } from "./util.ts";

const DROPBOX_TOKEN_LOCAL_STORAGE_KEY = 'TOKEN'
const API = 'https://api.dropboxapi.com'
const API_CONTENT = 'https://content.dropboxapi.com'
const CLIENT_ID = `1mxlmflbvrgdgdw`
const LOCAL_DENO_SERVER_PORT = 8080
const REDIRECT_URI = (typeof Deno !== 'undefined') ? `http://localhost:${LOCAL_DENO_SERVER_PORT}` : `${location.origin}${location.pathname}`.replace(/\/$/, '')
const CODE_VERIFIER = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
export const DROPBOX_AUTHORIZE_URL = `https://www.dropbox.com/oauth2/authorize?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&response_type=code&code_challenge_method=plain&code_challenge=${CODE_VERIFIER}`

let TOKEN: string;

async function getAuthorizationCode() {
    console.log(`Beginning call to /oauth2/authorize to obtain authorization code.`)

    let authorizationCode = '';

    console.log(`Click the following link to authorize this app to access your Dropbox account.\n`);
    console.log(`Once authorized, you can close the browser window.\n\n`);
    console.log(DROPBOX_AUTHORIZE_URL);
    console.log(`\n\n`);

    async function handle(conn: Deno.Conn) {
        const httpConn = Deno.serveHttp(conn);
        for await (const requestEvent of httpConn) {
            authorizationCode = requestEvent.request.url.replace(/.*\/\?code=/, '')
            const bodyContent = `Your code (${authorizationCode}) has been passed back to the app.  Close this window.`
            await requestEvent.respondWith(new Response(bodyContent))
            httpConn.close()
        }
    }

    const server = Deno.listen({ port: 8080 });

    for await (const conn of server) {
        await handle(conn);
        server.close()
    }

    console.log(`Finished call to /oauth2/authorize.  authorization code === ${authorizationCode}`)
    return authorizationCode
}

async function getToken(code: string) {
    const res = await fetch(`${API}/oauth2/token`, {
        method: 'POST',
        body: new URLSearchParams({
            code,
            grant_type: 'authorization_code',
            redirect_uri: REDIRECT_URI,
            // TODO this string should be more 'secure', i guess
            code_verifier: CODE_VERIFIER,
            client_id: CLIENT_ID
        }).toString(),
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    });

    // status === 200   ->   token returned
    if (!(res.status === 200)) {
        throw new Error(await res.text());
    }

    return (<{ access_token: string }>(await res.json())).access_token
}

/**
 *
 * @param path
 * @returns true if folder created.  false if folder already exists
 */
export async function postCreateFolder(path: string) {
    // TODO make sure path begins with / in a more robust fashion
    path = `/${path}`

    const res = await fetch(`${API}/2/files/create_folder_v2`, {
        method: 'POST',
        body: JSON.stringify({ path, autorename: false }),
        headers: {
            'Authorization': `Bearer ${TOKEN}`,
            'Content-Type': 'application/json'
        },
    });

    // status === 200   ->   folder created
    // status === 409   ->   folder already exists
    if (!(res.status === 200 || res.status === 409)) {
        throw new Error(await res.text());
    }

    return res.status === 200
}

/**
 *
 * @param path
 * @returns true if folder created.  false if folder already exists
 */
export async function getFileList(contestName: string) {
    const path = `/${contestName}`

    const res = await fetch(`${API}/2/files/list_folder`, {
        method: 'POST',
        body: JSON.stringify({ path, limit: 2000 }),
        headers: {
            'Authorization': `Bearer ${TOKEN}`,
            'Content-Type': 'application/json'
        },
    });

    // status === 200   ->   folder created
    if (!(res.status === 200)) {
        throw new Error(await res.text());
    }

    return (<{ entries: Array<{ '.tag': string, path_display: string }> }>(await res.json())).entries.filter(e => e['.tag'] === 'file').map(e => e.path_display)
}

export async function addFile(contestName: string, body: string) {
    await delay(1 * 1000)

    const res = await fetch(`${API_CONTENT}/2/files/upload`, {
        method: 'POST',
        body,
        headers: {
            'Authorization': `Bearer ${TOKEN}`,
            'Content-Type': 'application/octet-stream',
            'Dropbox-API-Arg': JSON.stringify({
                path: `/${contestName}/${(new Date()).toISOString().replaceAll(':', '')}.txt`,
                mode: 'add',
                autorename: true
            }),
        },
    });

    // status === 200   ->   file uploaded
    if (!(res.status === 200)) {
        throw new Error(await res.text());
    }

    return await res.json()
}

export async function bulkMoveFiles(entries: Array<{ from_path: string, to_path: string }>) {
    await delay(3 * 1000)

    const res = await fetch(`${API}/2/files/move_batch_v2`, {
        method: 'POST',
        body: JSON.stringify({ entries }),
        headers: {
            'Authorization': `Bearer ${TOKEN}`,
            'Content-Type': 'application/json'
        },
    });

    // status === 200   ->   file moved
    if (!(res.status === 200)) {
        throw new Error(await res.text());
    }

    const result = await res.json() as { '.tag': 'async_job_id', async_job_id: 'string' } | { '.tag': 'complete', entries: Array<{ '.tag': string }> }

    if (result['.tag'] === 'async_job_id') {
        await checkBatchJob(result.async_job_id)
    }
    // TODO do i need to mess with 'complete'

    return result
}

async function checkBatchJob(async_job_id: string): Promise<'complete' | 'in_progress'> {

    let jobStatus: 'complete' | 'in_progress'
    let res: Response;

    do {
        console.log('[DROPBOX] Waiting 5 seconds to check for completed batch job.')
        await delay(5 * 1000)
        res = await fetch(`${API}/2/files/move_batch/check_v2`, {
            method: 'POST',
            body: JSON.stringify({ async_job_id }),
            headers: {
                'Authorization': `Bearer ${TOKEN}`,
                'Content-Type': 'application/json'
            },
        })

        if (res.status !== 200) {
            throw new Error(await res.text())
        }

        jobStatus = (await res.json() as { '.tag': 'complete' | 'in_progress' })['.tag']
    } while (jobStatus === 'in_progress')

    return jobStatus
}

/**
 *
 * @param path
 * @returns contents of the file
 */
export async function getFile(path: string) {
    // TODO make sure path begins with / in a more robust fashion
    path = `${path}`

    const res = await fetch(`${API_CONTENT}/2/files/download`, {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${TOKEN}`,
            'Dropbox-API-Arg': JSON.stringify({ path }),
        },
    });

    // status === 200   ->   file uploaded
    if (!(res.status === 200)) {
        throw new Error(await res.text());
    }

    return await res.text()
}

/**
 * https://www.dropbox.com/developers/documentation/http/documentation#check-user
 * @param query The string to be sent to the API which is returned in the response.
 * @returns true if the request was successful, false otherwise
 */
async function postCheckUser(query = '🦃') {
    const res = await fetch(`${API}/2/check/user`, {
        method: 'POST',
        body: JSON.stringify({ query }),
        headers: {
            'Authorization': `Bearer ${TOKEN}`,
            'Content-Type': 'application/json'
        },
    });

    const result: { result: string } = await res.json()

    return res.ok &&
        res.status === 200 &&
        result.result === query
}

export async function authenticate() {
    console.log('[DROPBOX] Begin authentication process.')

    TOKEN = localStorage.getItem(DROPBOX_TOKEN_LOCAL_STORAGE_KEY) || ''

    if (!TOKEN) {
        const authCode = (typeof Deno !== 'undefined') ? await getAuthorizationCode() : location.search.replace(/^\?code=/, '')
        TOKEN = await getToken(authCode)
        localStorage.setItem(DROPBOX_TOKEN_LOCAL_STORAGE_KEY, TOKEN)
    }

    if (!(await postCheckUser())) {
        localStorage.removeItem(DROPBOX_TOKEN_LOCAL_STORAGE_KEY)
        throw new Error("[DROPBOX] Token is no good.  Try again.");
    }

    console.log('[DROPBOX] Successfully authenticated to Dropbox.')
}

const bestCache = new Map<string, Array<string>>()

export async function getCanonicalBestSolutions(contestName: string): Promise<Array<string>> {

    console.log(`[DROPBOX] Getting canonical bests for ${contestName}`)

    // Check the cache for bests.  Return if found.
    let bestForContest: Array<string> = bestCache.get(contestName) || []
    if (bestForContest.length > 0) {
        console.log(`[DROPBOX] Canonical bests for ${contestName} found in cache`)
        return bestForContest
    }

    const c = CONTEST_CLASS_MAPPING.get(contestName)

    if (c === undefined) {
        throw new Error(`[DROPBOX] No contest found named ${contestName}`)
    }

    // Make sure folder exists, or create it.
    await postCreateFolder(contestName)

    const allDropboxSolutions: Array<string> = []

    // Retrieve list of all Dropbox files for the contest.
    const solutionFiles = await getFileList(contestName)

    // Download each Dropbox file for contest.
    for (const file of solutionFiles) {
        console.log(`[DROPBOX] Downloading ${file}`)
        allDropboxSolutions.push(...(await getFile(file)).split('\n'))
    }

    // Use static class method to simplify all solutions into a single array of bests.
    console.log(`[DROPBOX] Getting canonical solutions for ${contestName} based on all downloaded files`)
    bestForContest = c.getCanonicalSolutions(allDropboxSolutions)
    bestCache.set(contestName, bestForContest)

    // If the simplified bests doesn't exactly match what was downloaded from Dropbox,
    //   save it to Dropbox and backup old solutions.
    if (!(bestForContest.length === allDropboxSolutions.length &&
        bestForContest.every(a => allDropboxSolutions.find(b => a === b)))
    ) {  // there is a new 'canonical best'
        console.log(`[DROPBOX] Saving new canonical best file for ${contestName}`)
        await saveSolution(contestName, bestForContest.join('\n'))
        console.log(`[DROPBOX] Saved canonical best, now backing up old files.`)
        await bulkMoveFiles(solutionFiles.map(f => { return { from_path: f, to_path: f.replace(`${contestName}/`, `${contestName}/backup/`) } }))
        console.log(`[DROPBOX] Backup of ${solutionFiles.length} files for ${contestName} is complete`)
    } else {
        console.log(`[DROPBOX] Previous canonical best file has not changed`)
    }

    return bestForContest
}

export async function saveSolution(contestName: string, solution: string) {
    console.log(`[DROPBOX] Saving solution of length ${solution.length} to Dropbox for ${contestName}`)
    let successful: boolean
    do {
        successful = true
        try {
            await addFile(contestName, solution)
        } catch (error) {
            successful = false;
            console.error(`[DROPBOX] Error saving solution: ${error}`)
        }
    } while (!successful)
    console.log(`[DROPBOX] Successfully saved solution of length ${solution.length} to Dropbox for ${contestName}`)
}
