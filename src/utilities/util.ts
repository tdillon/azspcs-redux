// import { Contest as ContestMT } from './contests/monochromatic-triangles.ts';
import { Contest as ContestAM } from '../contests/ap-math.ts'
import { Contest as ContestSS } from '../contests/stepping-stones.ts'
import { Contest as ContestOT } from '../contests/oddly-triangular.ts'
import { ContestClass } from '../contests/i-contest.ts'

export const CONTEST_CLASS_MAPPING = new Map<string, ContestClass>([
    ['ap-math', ContestAM],
    ['stepping-stones', ContestSS],
    ['oddly-triangular', ContestOT],
]);

export const CONTESTS = Object.freeze([...CONTEST_CLASS_MAPPING.keys()])
