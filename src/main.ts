import { App } from './app.ts'
import { VERSION } from './version.ts'
import { URLPattern as BOB } from './deps.web.ts'

// https://developer.mozilla.org/en-US/docs/Web/API/URLPattern
// Once Firefox implments UrlPatten, remove this:
// @ts-ignore: Property 'UrlPattern' does not exist
if (!globalThis.URLPattern) {
  // @ts-ignore: Property 'UrlPattern' does not exist
  globalThis.URLPattern = BOB
}

document.title = `AZsPCs Redux - ${VERSION}`
document.body.appendChild(new App())
