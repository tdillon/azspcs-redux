import { CONTESTS } from './utilities/util.ts'
import { VERSION } from './version.ts'
import { parse, exists } from './deps.ts'
import { authenticate, saveSolution, getCanonicalBestSolutions } from './utilities/dropbox.ts'
import { IClientMessage, IWorkerMessage } from './sw.ts'
import { HELP } from './utilities/help.ts'

const parsedArgs = parse(Deno.args)

if (parsedArgs.help || parsedArgs._[0] === 'help') {
    console.log(HELP)
    Deno.exit()
} else if (parsedArgs.version || parsedArgs._[0] === 'version') {
    console.log(`azspcs ${VERSION}`)
    Deno.exit()
} else if (parsedArgs._.length !== 1) {
    console.error(`[CLI] Invalid arguments.  Review the help documentation.  'azspcs help'`)
    Deno.exit(1)
} else if (parsedArgs._[0] === 'bests') {
    if (!Object.keys(parsedArgs).filter(k => k !== '_').every(k => CONTESTS.some(c => c === k))) {
        console.error('[CLI] Invalid parameter for bests command.')
        console.error(`[CLI] Allowed options: ${CONTESTS.map(c => `--${c}`).join(' ')}`)
        console.error(`[CLI] You provided: ${Object.keys(parsedArgs).filter(k => k !== '_').map(a => `--${a}`).join(' ')}`)
        Deno.exit(1)
    }

    await authenticate()

    for (const contest of CONTESTS) {
        if (parsedArgs[contest]) {
            console.log(`[CLI] Get best solutions for ${contest}`)
            console.log((await getCanonicalBestSolutions(contest)).join('\n\n;\n\n'))
        }
    }

    Deno.exit()
} else if (parsedArgs._[0] === 'run') {
    if (!Object.keys(parsedArgs).filter(k => k !== '_').every(k => CONTESTS.some(c => c === k))) {
        console.error('[CLI] Invalid parameter for run command.')
        console.error(`[CLI] Allowed options: ${CONTESTS.map(c => `--${c}`).join(' ')}`)
        console.error(`[CLI] You provided: ${Object.keys(parsedArgs).filter(k => k !== '_').map(a => `--${a}`).join(' ')}`)
        // TODO check the arg for the contest
        Deno.exit(1)
    }

    await authenticate()

    for (const contest of CONTESTS) {
        if (parsedArgs[contest]) {
            console.log(`[CLI] Find solutions for ${contest} - ${parsedArgs[contest]}`)

            const bestSolutionsForContest = await getCanonicalBestSolutions(contest)

            for (const arg of typeof parsedArgs[contest] === 'number' ? [parsedArgs[contest]] : parsedArgs[contest]) {
                console.log(`[CLI] Add worker for ${contest} - ${arg}`)

                // Clean this up if Deno implements https://github.com/denoland/deno/issues/8654
                const w = new Worker(await exists('./src/worker.ts')
                    ? new URL("./worker.ts", import.meta.url).href
                    : `https://gitlab.com/api/v4/projects/tdillon%2fazspcs-redux/packages/generic/deno-cli/${VERSION}/worker.js`, { type: "module" });

                w.addEventListener('message', async (e: MessageEvent<IWorkerMessage>) => {
                    if (e.data.type === 'INFO') {
                        console.log(`[CLI] Message from worker: ${e.data.message}`)
                    } else if (e.data.type === 'SOLUTION') {
                        console.log(`[CLI] Found new best for ${e.data.contest}(${e.data.n})==${e.data.score}  ->  ${e.data.solution}`)
                        await saveSolution(contest, e.data.solution)
                    } else {
                        throw new Error(`[CLI] message event was received from worker with bad data:  ${JSON.stringify(e.data)}`)
                    }
                })

                w.postMessage(<IClientMessage>{ contest, arg, bests: bestSolutionsForContest })
            }
        }
    }
} else {
    console.error(console.error(`Invalid arguments.  Review the help documentation.  'azspcs help'`))
    Deno.exit(1)
}