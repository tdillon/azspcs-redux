import { ContestClass, IContest } from './i-contest.ts';

export type RowColumnTuple = [row: number, column: number]

export const Contest: ContestClass = class Contest implements IContest {

    // export class Contest implements IContest {
    #n: number
    #score = 0
    #grid: Array<Array<boolean>>
    #myBest = 0

    static readonly NAME = 'AP Math'
    static readonly N = [2, 6, 11, 18, 27, 38, 50, 65, 81, 98, 118, 139, 162, 187, 214, 242, 273, 305, 338, 374, 411, 450, 491, 534, 578]
    static readonly KNOWN_BESTS = [6, 33, 80, 153, 266, 420, 621, 844, 1193, 1512, 1973, 2418, 2921, 3525, 4284, 5057, 5831, 6753, 7783, 8962, 10062, 11123, 12543, 14108, 15643]

    constructor();
    constructor(n: number);
    constructor(n: number, bests: string[]);
    constructor(s: string);
    constructor(arg?: number | string, bests?: string[]) {

        let solution: Array<Array<number>> | undefined

        if (typeof arg === 'undefined') {
            this.#n = Contest.N[0]
        } else if (typeof arg === 'number') {
            if (!Contest.N.some(value => arg === value)) {
                throw new Error(`[${Contest.NAME}] Invalid argument [${arg}] for constructor.  Valid values are ${Contest.N.join(', ')}.`)
            }
            this.#n = arg
        } else if (typeof arg === 'string') {  // e.g., {},{1,3}, {1,2,4}, { 1},{2}
            solution = <Array<Array<number>>>JSON.parse(`[${arg.replaceAll('{', '[').replaceAll('}', ']')}]`)
            this.#n = (solution.length + 1) / 2
        } else {
            throw new Error(`[${Contest.NAME}] Constructor used incorrectly`)
        }

        this.#grid = []
        for (let r = 0; r < this.#n * 2 - 1; ++r) {
            const row: Array<boolean> = Array(
                r < this.#n ?
                    r + this.#n :
                    r + this.#n - 2 * (r - this.#n + 1)
            ).fill(false)

            this.#grid.push(row)
        }

        // if bests are provided, i want to set this to be the best
        // this is in hopes of improving on current score, not a best practice for future contests
        if (bests) {
            const bestSolution = Contest.getSolution(this.#n, bests)
            solution = <Array<Array<number>>>JSON.parse(`[${bestSolution.replaceAll('{', '[').replaceAll('}', ']')}]`)
            this.#myBest = Contest.getBestScore(this.#n, [bestSolution])
        }

        if (solution && solution.length > 0) {
            for (let r = 0; r < this.#grid.length; ++r) {
                for (let c = 0; c < solution[r].length; ++c) {
                    this.#grid[r][solution[r][c]] = true
                    ++this.#score
                }
            }
        }

        if (!this.isValid) {
            throw new Error(`[${Contest.NAME}] Invalid grid`)
        }

    }

    static getCanonicalSolutions(noncanonicalSolutions: Array<string>): Array<string> {
        const allSolutions = noncanonicalSolutions.map(sol => new Contest(sol))
        const cononimcal = new Map<number, Contest>()
        for (const iterator of allSolutions) {
            const c = cononimcal.get(iterator.#n)
            if (c === undefined || iterator.score > c.score) {
                cononimcal.set(iterator.#n, iterator)
            }
        }

        return [...cononimcal.values()].map(c => c.toString())
    }

    static getBestScore(n: number, bestSolutionsForContest: Array<string>): number {
        const bestSolution = Contest.getSolution(n, bestSolutionsForContest)
        return bestSolution ? [...bestSolution.matchAll(/\d+/g)].length : 0
    }

    static getSolution(n: number, bestSolutionsForContest: Array<string>): string {
        return bestSolutionsForContest.find(s => JSON.parse(`[${s.replaceAll('{', '[').replaceAll('}', ']')}]`).length === 2 * n - 1) || ''
    }

    get isValid(): boolean {
        for (let rBegin = 0; rBegin < this.numRows; ++rBegin) {
            for (let cBegin = 0; cBegin < this.#grid[rBegin].length; ++cBegin) {
                if (this.#grid[rBegin][cBegin]) {
                    for (let rEnd = rBegin; rEnd < this.numRows; ++rEnd) {
                        for (let cEnd = rEnd === rBegin ? cBegin + 1 : 0; cEnd < this.#grid[rEnd].length; ++cEnd) {
                            if (this.#grid[rEnd][cEnd]) {
                                const mp = this.getMidpoint([rBegin, cBegin], [rEnd, cEnd])
                                if (mp && this.#grid[mp[0]][mp[1]]) {
                                    return false
                                }
                            }
                        }
                    }
                }
            }
        }
        return true
    }

    get numRows(): number {
        return this.#grid.length
    }

    get score(): number {
        return this.#score
    }

    getScore() {
        return this.#score;
    }

    /** Set all values in grid to false.  Zero the score. */
    reset() {
        this.#score = 0
        for (let r = 0; r < this.#grid.length; ++r) {
            for (let c = 0; c < this.#grid[r].length; ++c) {
                this.#grid[r][c] = false
            }
        }
    }

    numBadSolves = 0;

    solve() {
        let foundBetterSolution = false

        let randomRow: number
        let randomCol: number

        for (let i = 0; i < 100; ++i) {
            do {
                // TODO this could be an infinite loop
                randomRow = Math.floor(Math.random() * (this.#n * 2 - 1));
                randomCol = Math.floor(Math.random() * this.#grid[randomRow].length)
            } while (this.#grid[randomRow][randomCol])

            if (this.isValidMove(randomRow, randomCol)) {
                this.#grid[randomRow][randomCol] = true
                this.#score += 1
            }
        }

        if (this.#score > this.#myBest) {
            foundBetterSolution = true
            this.#myBest = this.#score
        }

        // BUG this doesn't work ask expected, once the solver hits the ceiling, this logic won't let it build back up (since it compares to this.#myBest)
        if (!foundBetterSolution && ++this.numBadSolves > 100) {
            this.reset()
            this.numBadSolves = 0
        }

        return foundBetterSolution
    }

    private isValidMove(i: number, j: number) {
        // TODO we can optimize this to only check where i,j is impacted
        const initialValue = this.#grid[i][j]
        this.#grid[i][j] = true
        const isValid = this.isValid
        this.#grid[i][j] = initialValue
        return isValid
    }

    /**
     * The 'order' of `begin` and `end` do not matter.
     * @param begin - Tuple [row,column] which is the index into the grid of one segment endpoint
     * @param end - Tuple [row,column] which is the index into the grid of one segment endpoint
     * @returns undefined when no midpoint exists, otherwise the midpoint as a [row, column] tuple
     */
    private getMidpoint(begin: RowColumnTuple, end: RowColumnTuple): RowColumnTuple | undefined {
        const beginCartRow = begin[0]
        const beginCartCol = begin[1] * 2 + Math.abs(this.#n - begin[0] - 1)
        const endCartRow = end[0]
        const endCartCol = end[1] * 2 + Math.abs(this.#n - end[0] - 1)
        const midCartRow = Math.min(beginCartRow, endCartRow) + Math.abs(beginCartRow - endCartRow) / 2
        const midCartCol = Math.min(beginCartCol, endCartCol) + Math.abs(beginCartCol - endCartCol) / 2

        if (Math.abs(beginCartRow - endCartRow) % 2 === 0  // row diff must be % 2 === 0
            &&
            (
                (this.#n % 2 === 0  // n is even
                    &&
                    (
                        (midCartRow % 2 === 1 && midCartCol % 2 === 0)
                        ||
                        (midCartRow % 2 === 0 && midCartCol % 2 === 1)
                    )
                )
                ||
                (
                    (this.#n % 2 === 1  // n is odd
                        &&
                        (
                            (midCartRow % 2 === 0 && midCartCol % 2 === 0)
                            ||
                            (midCartRow % 2 === 1 && midCartCol % 2 === 1)
                        ))
                )
            )
            &&
            !(begin[0] === end[0] && begin[1] === end[1])  // no midpoint when begin === end
        ) {
            return [midCartRow, (midCartCol - Math.abs(this.#n - midCartRow - 1)) / 2]
        }

        return undefined
    }

    toString(fancy = false) {
        return fancy ? [
            ...this.#grid.map((row, index) => [
                ' '.repeat(Math.abs(this.#n - index - 1)),
                '{',
                row.map(a => a ? '•' : '░').join(' '),
                '}',
                '\n',
            ].join('')),
        ].join('') :
            JSON.stringify(this.#grid.map(row => row.map((c, i) => c ? i : undefined).filter(c => c !== undefined))).replaceAll('[', '{').replaceAll(']', '}').replace(/^{/, '').replace(/}$/, '');
    }
}



if (import.meta.main) {
    const x = new Contest(11)

    console.log(x.toString(true))
    do {
        if (x.solve()) {
            console.log(x.getScore())
            console.log(x.toString(true))
        }
    } while (true)
}
