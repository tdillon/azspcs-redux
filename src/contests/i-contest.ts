// This seems super gross, thanks TypeScript
// https://github.com/microsoft/TypeScript/issues/14600
// https://github.com/microsoft/TypeScript/issues/34516
// https://github.com/microsoft/TypeScript/issues/33892
// https://www.typescriptlang.org/docs/handbook/interfaces.html#difference-between-the-static-and-instance-sides-of-classes


/**
 * Explain what this is for?
 */
export interface ContestClass {  // TODO rename ContestStaticInterface
    /** TODO EXPLAIN CONSTRUCTOR USAGE!!! */
    new(s: string) : IContest
    new(n?: number, bests?: Array<string>): IContest

    /**
     * The friendly name for the contest.
     * @example
     * My Contest Name
     */
    readonly NAME: string
    readonly N: Array<number>
    readonly KNOWN_BESTS: Array<number>

    /**
     * This method is used to find the best solutions for each `n` from the solutions provided in `noncanonicalSolutions`.
     * @param noncanonicalSolutions Array of AZSPCS solution strings.
     * Solutions can exist any `n`.
     * Solutions for some `n` may not exist.
     * Multiple solutions for any `n` can exist.
     * @return An array which contains at most 1 solution for each `n`.
     * The solution for each `n` must be the best out of all solutions for `n` provided in `noncanonicalSolutions`.
     */
    getCanonicalSolutions(noncanonicalSolutions: Array<string>): Array<string>

    /**
     * Return the score of the `n` problem contained in `bestSolutionsForContest`.
     * @param n EXPLAIN!
     * @param bestSolutionsForContest EXPLAIN!
     */
    getBestScore(n: number, bestSolutionsForContest: Array<string>): number

    /**
     * Return the AZSPCS solution string for problem `n` from `bestSolutionsForContest`.
     * @param n The problem number for the contest.
     * @param bestSolutionsForContest Array of AZSPCS solution strings.
     * Solutions can exist any `n`.
     * Solutions for some `n` may not exist.
     * At most 1 solution for any `n` can exist.
     * @return The AZSPCS solution string for problem `n` from `bestSolutionsForContest`.
     * If not found, returns empty string.
     */
    getSolution(n: number, bestSolutionsForContest: Array<string>): string
}

/**
 * TypeScript (i.e., maintainers) stink.
 *
 * Since TypeScript doesn't allow an interface to include static items,
 * you'll need to implement the following
 *
 * @example
 * TODO
 */
export interface IContest {  // TODO rename to ContestInstanceInterface

    /**
     * @returns true if a better solution was found; false otherwise
     */
    solve(): boolean;

    /**
     * TODO EXPLAIN!
     */
    getScore(): number;

    /**
     * TODO EXPLAIN!
     * @param fancy EXPLAIN!
     */
    toString(fancy?: boolean): string;
}

