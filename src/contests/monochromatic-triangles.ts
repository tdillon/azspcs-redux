import { IContest } from "./i-contest.ts";

export class Contest  implements IContest {
    private triangles: Array<[number, number, number]>;
    private colorings: Array<Array<0 | 1 | 2>>;
    private best = Number.MAX_SAFE_INTEGER
    static readonly DEFAULT_N = 7
    static readonly NAME = 'Monochromatic Triangles'
    static readonly N = []
    static readonly KNOWN_BESTS = []

    constructor(private numNodes: number = Contest.DEFAULT_N) {
        this.triangles = []
        this.colorings = []

        for (let i = 0; i < numNodes; ++i) {
            this.colorings.push([])

            for (let j = i + 1; j < numNodes; ++j) {
                this.colorings[i][j] = 0;
            }
        }

        for (let i = 0; i < numNodes; ++i) {
            for (let j = i + 1; j < numNodes; ++j) {
                for (let k = j + 1; k < numNodes; ++k) {
                    this.triangles.push([i, j, k])
                }
            }
        }
    }

    static getCanonicalSolutions(noncanonicalSolutions:Array<string>):Array<string> {
        for (const solution of noncanonicalSolutions) {
            console.log(solution)
            // TODO figure out score
            // TODO compare to existing best (or set as best if non-existant)
        }
        return []
    }

    solve() {
        this.randomizeColorings()

        if (this.getScore() < this.best) {
            this.best = this.getScore()
            return true
        }

        return false
    }

    randomizeColorings() {
        for (let i = 0; i < this.numNodes; ++i) {
            for (let j = i + 1; j < this.numNodes; ++j) {
                this.colorings[i][j] = <0 | 1 | 2>Math.round(Math.random() * 2);
            }
        }
    }

    allBetterColorings() {
        let foundBetter = false
        let currentScore = this.getScore()

        for (let i = 0; i < this.numNodes; ++i) {
            for (let j = i + 1; j < this.numNodes; ++j) {
                const origColor = this.colorings[i][j]
                this.colorings[i][j] = <0 | 1 | 2>Math.round(Math.random() * 2);
                if (this.getScore() < currentScore) {
                    currentScore = this.getScore()
                    foundBetter = true
                } else {
                    this.colorings[i][j] = origColor
                }
            }
        }

        return foundBetter
    }

    addNode() {
        this.colorings.push([])

        ++this.numNodes

        for (let i = 0; i < this.colorings.length - 1; ++i) {
            this.colorings[i][this.colorings.length - 1] = 0;
        }

        for (let i = 0; i < this.numNodes; ++i) {
            for (let j = i + 1; j < this.numNodes; ++j) {
                this.triangles.push([i, j, this.numNodes - 1])
            }
        }
    }

    optimizeLastNode() {
        let foundBetter = false
        let currentScore = this.getScore()

        for (let i = 0; i < this.colorings.length - 1; ++i) {
            const origColor = this.colorings[i][this.colorings.length - 1]
            this.colorings[i][this.colorings.length - 1] = <0 | 1 | 2>Math.round(Math.random() * 2)
            if (this.getScore() < currentScore) {
                currentScore = this.getScore()
                foundBetter = true
            } else {
                this.colorings[i][this.colorings.length - 1] = origColor
            }
        }

        return foundBetter
    }


    getScore() {
        return this.triangles.reduce((prevVal, triangle) =>
            prevVal + ((this.colorings[triangle[0]][triangle[1]] ===
                this.colorings[triangle[0]][triangle[2]] &&
                this.colorings[triangle[0]][triangle[2]] ===
                this.colorings[triangle[1]][triangle[2]]) ? 1 : 0)
            , 0)
    }

    public toString() {
        const retVal: Array<string> = [];

        for (let i = 0; i < this.numNodes; ++i) {

            for (let j = 0; j < this.numNodes; ++j) {
                retVal.push(isNaN(this.colorings[j][i]) ? '' : this.colorings[j][i].toString())
            }
            retVal.push(',')
        }

        return retVal.join('').replaceAll(/^,(.*),$/g, '$1')
    }
}
