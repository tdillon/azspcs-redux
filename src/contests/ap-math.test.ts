import { assertEquals, assertThrows } from '../deps.ts'
import { Contest, RowColumnTuple } from './ap-math.ts'

Deno.test(`example solutions`, () => {
    let c = new Contest('{},{1,3},{1,2,4},{1},{2}')
    assertEquals(c.getScore(), 7, `The score of \n${c.toString()} should be 7, but [${c.getScore()}] was returned.`)

    c = new Contest('{2},{1,3},{2},{1,3},{1,2}')
    assertEquals(c.getScore(), 8, `The score of \n${c.toString()} should be 8, but [${c.getScore()}] was returned.`)

    c = new Contest('{0,1},{0,2},{0,1}')
    assertEquals(c.getScore(), 6, `The score of \n${c.toString()} should be 8, but [${c.getScore()}] was returned.`)

    assertThrows(() => c = new Contest('{1},{1,2},{0,2,3},{1,3},{2}'))

    assertThrows(() => c = new Contest('{0,1,3,4},{4,6},{0,1,6},{1,2,5},{},{1,7,8,10},{6,9},{1,3,7},{1,2,7},{0},{0,1,3,4}'))
});

Deno.test(`midpoint - n=2`, () => {
    const c = new Contest(2)
    const valid: Array<[RowColumnTuple, RowColumnTuple | undefined, RowColumnTuple]> = [
        [[0, 0], undefined, [2, 0]],
        [[0, 0], [1, 1], [2, 1]],
        [[0, 1], [1, 1], [2, 0]],
        [[1, 0], [1, 1], [1, 2]],
    ]
    valid.forEach(segment => {
        //@ts-ignore getMidpoint is private
        assertEquals(c.getMidpoint(segment[0], segment[2]), segment[1], `The midpoint of segment [${segment[0]}]->[${segment[2]}] should be [${segment[1]}] but [${c.getMidpoint(segment[0], segment[2])}] was returned.`)
        //@ts-ignore getMidpoint is private
        assertEquals(c.getMidpoint(segment[2], segment[0]), segment[1], `The midpoint of segment [${segment[2]}]->[${segment[0]}] should be [${segment[1]}] but [${c.getMidpoint(segment[2], segment[0])}] was returned.`)
    });
})

Deno.test(`midpoint - n=3`, () => {
    const c = new Contest('{},{},{},{},{}')
    const valid: Array<[RowColumnTuple, RowColumnTuple | undefined, RowColumnTuple]> = [
        [[1, 0], undefined, [3, 0]],
    ]
    valid.forEach(segment => {
        //@ts-ignore getMidpoint is private
        assertEquals(c.getMidpoint(segment[0], segment[2]), segment[1], `The midpoint of segment [${segment[0]}]->[${segment[2]}] should be [${segment[1]}] but [${c.getMidpoint(segment[0], segment[2])}] was returned.`)
        //@ts-ignore getMidpoint is private
        assertEquals(c.getMidpoint(segment[2], segment[0]), segment[1], `The midpoint of segment [${segment[2]}]->[${segment[0]}] should be [${segment[1]}] but [${c.getMidpoint(segment[2], segment[0])}] was returned.`)
    });
})

Deno.test(`midpoint - n=4`, () => {
    const c = new Contest('{},{},{},{},{},{},{}')
    const valid: Array<[RowColumnTuple, RowColumnTuple | undefined, RowColumnTuple]> = [
        [[2, 0], undefined, [4, 0]],
    ]
    valid.forEach(segment => {
        //@ts-ignore getMidpoint is private
        assertEquals(c.getMidpoint(segment[0], segment[2]), segment[1], `The midpoint of segment [${segment[0]}]->[${segment[2]}] should be [${segment[1]}] but [${c.getMidpoint(segment[0], segment[2])}] was returned.`)
        //@ts-ignore getMidpoint is private
        assertEquals(c.getMidpoint(segment[2], segment[0]), segment[1], `The midpoint of segment [${segment[2]}]->[${segment[0]}] should be [${segment[1]}] but [${c.getMidpoint(segment[2], segment[0])}] was returned.`)
    });
})

Deno.test(`midpoint - n=5`, () => {
    const c = new Contest('{},{},{},{},{},{},{},{},{}')  // Test with n=5
    const valid: Array<[RowColumnTuple, RowColumnTuple | undefined, RowColumnTuple]> = [

        // three transposed vertical segments |
        [[0, 2], [2, 3], [4, 4]], [[2, 3], [4, 4], [6, 3]], [[4, 4], [6, 3], [8, 2]],
        [[0, 3], [2, 4], [4, 5]], [[2, 4], [4, 5], [6, 4]], [[4, 5], [6, 4], [8, 3]],

        // two transposed vertical segments |
        [[1, 2], [3, 3], [5, 3]], [[3, 3], [5, 3], [7, 2]],
        [[1, 3], [3, 4], [5, 4]], [[3, 4], [5, 4], [7, 3]],

        // three transposed horizontal segments -
        [[3, 3], [3, 4], [3, 5]],
        [[4, 3], [4, 4], [4, 5]],
        [[5, 3], [5, 4], [5, 5]],

        // transposed diagonals /
        [[1, 3], [2, 3], [3, 3]],
        [[2, 3], [3, 3], [4, 3]],
        [[3, 3], [4, 3], [5, 2]],
        [[4, 3], [5, 2], [6, 1]],
        [[5, 2], [6, 1], [7, 0]],

        // transposed diagonals \
        [[1, 1], [2, 2], [3, 3]],
        [[2, 2], [3, 3], [4, 4]],
        [[3, 3], [4, 4], [5, 4]],
        [[4, 4], [5, 4], [6, 4]],
        [[5, 4], [6, 4], [7, 4]],

        // odd row spanners -
        [[1, 2], [3, 2], [5, 1]], // /
        [[1, 2], [3, 3], [5, 3]], // |
        [[1, 2], [3, 4], [5, 5]], // \

        [[1, 2], [4, 3], [7, 1]], // /
        [[1, 2], [4, 4], [7, 3]], // \

        [[3, 3], [4, 3], [5, 2]], // /
        [[3, 3], [4, 4], [5, 4]], // \

        [[3, 3], [5, 2], [7, 0]], // /
        [[3, 3], [5, 3], [7, 2]], // |
        [[3, 3], [5, 4], [7, 4]], // \

        // ALL 0,0 segments
        [[0, 0], [0, 1], [0, 2]], [[0, 0], [0, 2], [0, 4]],
        [[0, 0], [1, 0], [2, 0]], [[0, 0], [1, 1], [2, 2]], [[0, 0], [1, 2], [2, 4]], [[0, 0], [1, 3], [2, 6]],
        [[0, 0], [2, 0], [4, 0]], [[0, 0], [2, 1], [4, 2]], [[0, 0], [2, 2], [4, 4]], [[0, 0], [2, 3], [4, 6]], [[0, 0], [2, 4], [4, 8]],
        [[0, 0], [3, 1], [6, 0]], [[0, 0], [3, 2], [6, 2]], [[0, 0], [3, 3], [6, 4]], [[0, 0], [3, 4], [6, 6]],
        [[0, 0], [4, 2], [8, 0]], [[0, 0], [4, 3], [8, 2]], [[0, 0], [4, 4], [8, 4]],

        // Random spots
        [[2, 2], [5, 4], [8, 4]],
        [[4, 4], [6, 4], [8, 4]],
        [[4, 4], [4, 2], [4, 0]],
        [[4, 4], [4, 5], [4, 6]],
        [[4, 4], [4, 6], [4, 8]],
        [[4, 4], [3, 5], [2, 6]],
        [[4, 4], [3, 3], [2, 2]],
        [[4, 4], [5, 3], [6, 2]],
        [[0, 1], [0, 2], [0, 3]],
        [[0, 1], [1, 1], [2, 1]],
        [[0, 2], [3, 5], [6, 6]],
        [[0, 2], [4, 4], [8, 2]],
        [[0, 3], [3, 3], [6, 1]],

        // Known bads
        [[3, 0], undefined, [5, 0]],
        [[0, 0], undefined, [0, 0]],
        [[0, 0], undefined, [2, 1]],
        [[4, 4], undefined, [6, 3]],
        [[2, 2], undefined, [2, 2]],
        [[0, 0], undefined, [1, 0]],
        [[0, 0], undefined, [6, 1]],
        [[0, 2], undefined, [6, 3]],

    ]
    valid.forEach(segment => {
        //@ts-ignore getMidpoint is private
        assertEquals(c.getMidpoint(segment[0], segment[2]), segment[1], `The midpoint of segment [${segment[0]}]->[${segment[2]}] should be [${segment[1]}] but [${c.getMidpoint(segment[0], segment[2])}] was returned.`)
        //@ts-ignore getMidpoint is private
        assertEquals(c.getMidpoint(segment[2], segment[0]), segment[1], `The midpoint of segment [${segment[2]}]->[${segment[0]}] should be [${segment[1]}] but [${c.getMidpoint(segment[2], segment[0])}] was returned.`)
    });
})

Deno.test(`midpoint - n=6`, () => {
    const c = new Contest(6)
    const valid: Array<[RowColumnTuple, RowColumnTuple | undefined, RowColumnTuple]> = [
        [[0, 0], [3, 5], [6, 9]],
    ]
    valid.forEach(segment => {
        //@ts-ignore getMidpoint is private
        assertEquals(c.getMidpoint(segment[0], segment[2]), segment[1], `The midpoint of segment [${segment[0]}]->[${segment[2]}] should be [${segment[1]}] but [${c.getMidpoint(segment[0], segment[2])}] was returned.`)
        //@ts-ignore getMidpoint is private
        assertEquals(c.getMidpoint(segment[2], segment[0]), segment[1], `The midpoint of segment [${segment[2]}]->[${segment[0]}] should be [${segment[1]}] but [${c.getMidpoint(segment[2], segment[0])}] was returned.`)
    });
})