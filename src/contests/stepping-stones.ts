import { ContestClass, IContest } from './i-contest.ts';

export type RowColumnTuple = [row: number, column: number]

export const Contest: ContestClass = class Contest implements IContest {
    static readonly NAME = 'Stepping Stones'
    static readonly N = [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
    static readonly KNOWN_BESTS = [71, 80, 89, 99, 109, 115, 124, 133, 144, 149, 156, 167, 173, 180, 185, 194, 200, 206, 215, 221, 230, 236, 249, 258, 265]
    static readonly #GRID_SIZE = 1000
    static readonly #CENTER_X = 500
    static readonly #CENTER_Y = 500
    #n: number
    #currentScore: number
    #myBestScore: number
    #grid!: Array<Array<number | null>>
    #items!: Array<{ k: number, x: number, y: number }>
    #numIs!: number

    constructor();
    constructor(n: number);
    constructor(n: number, bests: string[]);
    constructor(s: string);
    constructor(arg?: number | string, bests?: string[]) {

        if (typeof arg === 'undefined') {
            this.#n = Contest.N[0]
            this.#currentScore = 0
        } else if (typeof arg === 'number') {
            if (!Contest.N.some(value => arg === value)) {
                throw new Error(`[${Contest.NAME}] Invalid argument [${arg}] for constructor.  Valid values are ${Contest.N.join(', ')}.`)
            }
            this.#n = arg
            this.#currentScore = 0
        } else if (typeof arg === 'string') {  // A valid (assumed) AZSPCS solution string
            // TODO construct object based on arg
            this.#n = Contest.#getNFromSolution(arg)
            this.#currentScore = Contest.#getScoreFromSolution(arg)
        } else {
            throw new Error(`[${Contest.NAME}] Constructor used incorrectly`)
        }

        this.#myBestScore = Contest.#getScoreFromSolution(Contest.getSolution(this.#n, bests || []))
    }

    static getCanonicalSolutions(noncanonicalSolutions: Array<string>): Array<string> {
        const cononimcal = new Map<number, string>()
        for (const solution of noncanonicalSolutions) {
            const nnn = Contest.#getNFromSolution(solution)
            const currentBestSolution = cononimcal.get(nnn)
            const scoreForCurrentBestSolution = Contest.#getScoreFromSolution(currentBestSolution || '')
            const scoreForSolution = Contest.#getScoreFromSolution(solution)
            if (currentBestSolution === undefined || scoreForSolution > scoreForCurrentBestSolution) {
                cononimcal.set(nnn, solution)
            }
        }

        return [...cononimcal.values()]
    }

    static getBestScore(n: number, bestSolutionsForContest: Array<string>): number {
        const bestSolution = Contest.getSolution(n, bestSolutionsForContest)
        return [...bestSolution.matchAll(/\d+(?!:)/g)].reduce((p, c) => Math.max(p, +c[0]), 0)
    }

    static getSolution(n: number, bestSolutionsForContest: Array<string>): string {
        return bestSolutionsForContest.find(s => [...s.matchAll(/:1[/)]|[,:]1(?=,)|,1[/)]/g)].length === n) || ''
    }

    /** Get the score for the given AZSPCS solution string.  TODO: should this be in i-contest? */
    static #getScoreFromSolution(solution: string): number {
        return [...solution.matchAll(/\d+(?!:)/g)].reduce((p, c) => Math.max(p, +c[0]), 0)
    }

    /** Get the problem number for the given AZSPCS solution string.  TODO: should this be in i-contest? */
    static #getNFromSolution(solution: string): number {
        return [...solution.matchAll(/:1[/)]|[,:]1(?=,)|,1[/)]/g)].length
    }

    getScore(): number {
        return this.#currentScore
    }

    public solve() {
        this.#reset()
        this.#loadRandomOnes()

        let k = 2
        while (this.#makeMove(k++));
        this.#currentScore = k - 2

        if (this.#currentScore > this.#myBestScore) {
            this.#myBestScore = this.#currentScore
            return true
        }

        return false
    }

    toString(fancy = false) {
        const minX = this.#items.reduce((prev, cur) => Math.min(prev, cur.x), Number.MAX_SAFE_INTEGER)
        const maxX = this.#items.reduce((prev, cur) => Math.max(prev, cur.x), Number.MIN_SAFE_INTEGER)
        const minY = this.#items.reduce((prev, cur) => Math.min(prev, cur.y), Number.MAX_SAFE_INTEGER)
        const maxY = this.#items.reduce((prev, cur) => Math.max(prev, cur.y), Number.MIN_SAFE_INTEGER)

        if (fancy) {
            const str: Array<string> = []

            for (let y = maxY; y >= minY; --y) {
                str.push(`┼${'─────┼'.repeat(maxX - minX + 1)}\n`)
                for (let x = minX; x <= maxX; ++x) {
                    str.push(`│ ${(this.#grid[x][y] ?? '').toString().padStart(3, ' ')} `)
                }
                str.push('│\n')
            }
            str.push(`┼${'─────┼'.repeat(maxX - minX + 1)}\n`)

            return str.join('')
        } else {
            const solution: Array<string> = []
            for (let y = maxY; y >= minY; --y) {
                const xx: Array<number | null> = []
                for (let x = minX; x <= maxX; ++x) {
                    xx.push(this.#grid[x][y])
                }
                solution.push(Contest.#getSubmissionString(xx))
            }

            return solution.join(',')
        }
    }

    static #getSubmissionString(x: Array<number | null>) {
        let numSpaces = 0
        let retVal = ''
        let prevSpace = true

        for (let i = 0; i < x.length; ++i) {
            if (x[i] && prevSpace) {
                retVal += `${numSpaces}:${x[i]}`
                numSpaces = 0
                prevSpace = false
            } else if (x[i]) {
                retVal += `,${x[i]}`
                prevSpace = false
            } else if (!x[i] && !prevSpace) {
                retVal += '/'
                ++numSpaces
                prevSpace = true
            } else {
                ++numSpaces
                prevSpace = true
            }
        }

        return `(${retVal.replace(/\/$/, '')})`
    }


    #makeMove(k: number): boolean {
        const options = this.#items.flatMap(i => this.#getNeighbors(i))
            .filter(n => !this.#grid[n.x][n.y])
            .map(nn => ({
                ...nn,
                sum: this.#getNeighbors(nn).reduce((p, c) => p + (this.#grid[c.x][c.y] ?? 0), 0)
            }))
            .filter(nn => nn.sum === k)

        const option = options[(options.length * Math.random()) | 0]

        if (option) {
            this.#items.push({ k, x: option.x, y: option.y })
            this.#grid[option.x][option.y] = k
            return true
        } else {
            return false
        }
    }

    #getNeighbors(coord: { x: number, y: number }): Array<{ x: number, y: number }> {
        return [
            { x: coord.x - 1, y: coord.y - 1 },
            { x: coord.x - 1, y: coord.y },
            { x: coord.x - 1, y: coord.y + 1 },
            { x: coord.x, y: coord.y + 1 },
            { x: coord.x + 1, y: coord.y + 1 },
            { x: coord.x + 1, y: coord.y },
            { x: coord.x + 1, y: coord.y - 1 },
            { x: coord.x, y: coord.y - 1 }
        ]
    }


    #reset() {
        // TODO can this be optimized to just update #grid based on #items?
        this.#grid = []
        this.#items = []
        this.#numIs = 1

        for (let i = 0; i < Contest.#GRID_SIZE; ++i) {
            this.#grid[i] = []
        }
        this.#grid[Contest.#CENTER_X][Contest.#CENTER_Y] = 1
        this.#items.push({ k: 1, x: Contest.#CENTER_X, y: Contest.#CENTER_Y })

        this.#items.forEach(i => this.#grid[i.x][i.y] = i.k)

    }

    #loadRandomOnes() {
        for (let ring = 1; ; ++ring) {

            for (let x = Contest.#CENTER_X - ring; x <= Contest.#CENTER_X + ring; ++x) {
                if (Math.random() < (Math.pow(ring, 2) * .005)) {
                    this.#grid[x][Contest.#CENTER_Y - ring] = 1
                    this.#items.push({ k: 1, x: x, y: Contest.#CENTER_Y - ring })
                    if (++this.#numIs === this.#n) {
                        return
                    }
                }
            }

            for (let x = Contest.#CENTER_X - ring; x <= Contest.#CENTER_X + ring; ++x) {
                if (Math.random() < (Math.pow(ring, 2) * .005)) {
                    this.#grid[x][Contest.#CENTER_Y + ring] = 1
                    this.#items.push({ k: 1, x: x, y: Contest.#CENTER_Y + ring })
                    if (++this.#numIs === this.#n) {
                        return
                    }
                }
            }

            for (let y = Contest.#CENTER_Y - ring + 1; y <= Contest.#CENTER_Y + ring - 1; ++y) {
                if (Math.random() < (Math.pow(ring, 2) * .005)) {
                    this.#grid[Contest.#CENTER_X - ring][y] = 1
                    this.#items.push({ k: 1, x: Contest.#CENTER_X - ring, y: y })
                    if (++this.#numIs === this.#n) {
                        return
                    }
                }
            }

            for (let y = Contest.#CENTER_Y - ring + 1; y <= Contest.#CENTER_Y + ring - 1; ++y) {
                if (Math.random() < (Math.pow(ring, 2) * .005)) {
                    this.#grid[Contest.#CENTER_X + ring][y] = 1
                    this.#items.push({ k: 1, x: Contest.#CENTER_X + ring, y: y })
                    if (++this.#numIs === this.#n) {
                        return
                    }
                }
            }

        }
    }

}



if (import.meta.main) {
    const x = new Contest()

    do {
        if (x.solve()) {
            console.log(x.getScore())
            console.log(x.toString())
            console.log(x.toString(true))
        }
    } while (true)
}
