import { ContestClass, IContest } from './i-contest.ts';

export type RowColumnTuple = [row: number, column: number]

export const Contest: ContestClass = class Contest implements IContest {
    static readonly NAME = 'Oddly Triangular'
    static readonly N = [1]
    static readonly KNOWN_BESTS = [3_600_005.53]
    #n: number
    #currentScore: number
    #myBestScore: number

    constructor();
    constructor(n: number);
    constructor(n: number, bests: string[]);
    constructor(s: string);
    constructor(arg?: number | string, bests?: string[]) {

        if (typeof arg === 'undefined') {
            this.#n = Contest.N[0]
            this.#currentScore = 0
        } else if (typeof arg === 'number') {
            if (!Contest.N.some(value => arg === value)) {
                throw new Error(`[${Contest.NAME}] Invalid argument [${arg}] for constructor.  Valid values are ${Contest.N.join(', ')}.`)
            }
            this.#n = arg
            this.#currentScore = 0
        } else if (typeof arg === 'string') {  // A valid (assumed) AZSPCS solution string
            // TODO construct object based on arg
            this.#n = Contest.#getNFromSolution(arg)
            this.#currentScore = Contest.#getScoreFromSolution(arg)
        } else {
            throw new Error(`[${Contest.NAME}] Constructor used incorrectly`)
        }

        this.#myBestScore = Contest.#getScoreFromSolution(Contest.getSolution(this.#n, bests || []))
    }

    static getCanonicalSolutions(noncanonicalSolutions: Array<string>): Array<string> {
        const cononimcal = new Map<number, string>()
        for (const solution of noncanonicalSolutions) {
            const nnn = Contest.#getNFromSolution(solution)
            const currentBestSolution = cononimcal.get(nnn)
            const scoreForCurrentBestSolution = Contest.#getScoreFromSolution(currentBestSolution || '')
            const scoreForSolution = Contest.#getScoreFromSolution(solution)
            if (currentBestSolution === undefined || scoreForSolution > scoreForCurrentBestSolution) {
                cononimcal.set(nnn, solution)
            }
        }

        return [...cononimcal.values()]
    }

    static getBestScore(_n: number, bestSolutionsForContest: Array<string>): number {
        return Contest.#getScoreFromSolution(bestSolutionsForContest[0])
    }

    static getSolution(_n: number, bestSolutionsForContest: Array<string>): string {
        return bestSolutionsForContest[0] || ''
    }

    /** Get the score for the given AZSPCS solution string.  TODO: should this be in i-contest? */
    static #getScoreFromSolution(solution: string): number {
        return Math.log10(+solution)
    }

    /** Get the problem number for the given AZSPCS solution string.  TODO: should this be in i-contest? */
    static #getNFromSolution(_solution: string): number {
        return 1
    }

    getScore(): number {
        return this.#currentScore
    }

    public solve() {
        while (++this.#n && !(this.#n.toString().split('').every(d => +d % 2 !== 0) && (this.#n * (this.#n + 1) / 2).toString().split('').every(d => +d % 2 !== 0)));

        this.#currentScore = Contest.#getScoreFromSolution(this.#n.toString())

        return true
    }

    toString(_fancy = false) {
        return this.#n.toString()
    }


}



if (import.meta.main) {
    const x = new Contest()

    do {
        if (x.solve()) {
            console.log(x.getScore())
            console.log(x.toString())
            console.log(x.toString(true))
        }
    } while (true)
}
